const toJsonApiError = require('../../../../../lib/yus-jsonapi/yus-jsonapi').toJsonApiError;
const SB2Error = require('../../../../../src/helpers/sb2_error');

describe('yus-jsonapi', () => {
  describe('toJsonApiError function', () => {

    fit('should convert SB2Error into JSONAPI format error', () => {
      let errorObject = {
        "status": "400",
        "code": "72",
        "title": "Invalid content type",
        "detail": "Request contains an invalid content type",
        "links": ""
      };

      let jsonApiErrorObject = {
        errors: [{
          id: '',
          links: {
            about: ''
          },
          status: '400',
          code: '72',
          title: 'Invalid content type',
          detail: 'Request contains an invalid content type',
          source: {
            pointer: ''
          },
          meta: {
            extra: null
          }
        }]
      };
      
      let sb2error = new SB2Error(errorObject);
      sb2error.id = '';
      expect(toJsonApiError(sb2error)).toEqual(jsonApiErrorObject);

    })
  })


})
