console.log('test.js')
const validate = require('../yus-jsonapi').validate;

const validTests = require('./tests-valid.json');
const invalidTests = require('./tests-invalid.json');


describe('the validate method', function() {

  let validMethods = ['GET', 'POST', 'PUT', 'PATCH', 'DELETE']
  let invalidMethods = ['HEAD', 'CONNECT', 'OPTIONS', 'TRACE']
  let validEmptyMethods = ['GET', 'DELETE'];
  let createMethods = ['POST', 'PUT'];
  let updateMethods = ['PATCH']
  let body = {};
  let request = true;
  it('should return true is body is empty', function() {
    validEmptyMethods.forEach(function(method) {
      expect(validate(body,method,request)).toEqual(true);

    })
  })

  it('should ignore un-supported HTTP methods and return true', function() {
    invalidMethods.forEach(function(method) {
      expect(validate(body,method,request)).toEqual(true);
    })
  })

  // Setup spies on these
  it('should call createValidator if it is a create request ', function() {

  })

  it('should call updateValidator if it is a update request ', function() {

  })
  it('should call regularValidator if it is a any other request ', function() {

  })
  it('should call regularValidator if it is not a request (api response) ', function() {

  })

  // loop over every valid test case and validate it
  it('should return true if the json is correct ', function() {
    validTests.forEach(function(test) {
      expect(validate(test.body,test.method,test.request)).toEqual(true)
    })
  })

  // loop over every invalid test case and validate it
  // Error messages might need to be tested?
  it('should throw an error if the validator fails ', function() {
    invalidTests.forEach(function(test) {
      let errorCaller = function() {
        try {
          validate(test.body, test.method, test.request);
        } catch (error) {
          throw error;
        }
      }
      expect(errorCaller).toThrow()
    })
  })

})
