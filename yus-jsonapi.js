/* jshint node: true */
/* jshint esnext: true */

/**
 * URI's
 *
 * /channels
 * /channels?include=schedules
 * /channels?include=schedules.timeslots
 *
 * /channels/1
 * /channels/1?include=schedules
 * /channels/1?include=schedules.timeslots
 *
 * /channels/1/schedules
 * /channels/1/schedules?include=timeslots
 *
 * /channels/1/schedules/2
 * /channels/1/schedules?include=timeslots
 *
 * /channels/1/schedules/2/timeslots
 * /channels/1/schedules/2/timeslots/3
 *
 */

const _ = require('lodash');
const fs = require('fs');
const regularSchema = fs.readFileSync(__dirname + '/jsonapi-schema.json', 'utf8');
const updateSchema = fs.readFileSync(__dirname + '/jsonapi-schema-update.json', 'utf8');
const createSchema = fs.readFileSync(__dirname + '/jsonapi-schema-create.json', 'utf8');
const util = require('util');
const Validator = require('jsonapi-validator').Validator;
const regularValidator = new Validator(JSON.parse(regularSchema));
const updateValidator = new Validator(JSON.parse(updateSchema));
const createValidator = new Validator(JSON.parse(createSchema));
let API_ADDRESS;

function getBaseUrl() {
  return API_ADDRESS;
}

function getResourceIdentifierObjects(models) {
  return _.map(models, getResourceIdentifierObject);
}

function getResourceObjects(models) {
  return _.map(models, getResourceObject);
}

// http://stackoverflow.com/a/4648411/14966
Object.defineProperty(
  Object.prototype,
  'renameProperty', {
    writable: false, // Cannot alter this property
    enumerable: false, // Will not show up in a for-in loop.
    configurable: false, // Cannot be deleted via the delete operator
    value: function(oldName, newName) {
      // Do nothing if the names are the same
      if (oldName == newName) {
        return this;
      }
      // Check for the old property name to
      // avoid a ReferenceError in strict mode.
      if (this.hasOwnProperty(oldName)) {
        this[newName] = this[oldName];
        delete this[oldName];
      }
      return this;
    }
  }
);

// http://stackoverflow.com/a/4648411/14966
function getResourceObject(model) {
  let resourceObject = getResourceIdentifierObject(model);
  resourceObject.attributes = getAttributesObject(model);
  resourceObject.meta = getResourceMetaObject(model);
  resourceObject.links = getResourceLinksObject(model);

  if (hasRelationships(model)) {
    resourceObject.relationships = getRelationshipsObject(model);
  }
  return resourceObject;
}

function getResourceMetaObject(model) {
  if (model.meta) {
    return model.meta;
  }
}

function hasRelationships(model) {
  return _.keys(model.relationships).length;
}

function isRelationshipsEndpoint(req) {
  return _.includes(req.originalUrl, '/relationships/');
}


// http://jsonapi.org/format/#document-resource-identifier-objects
function getResourceIdentifierObject(model) {
  if (!model) throw new Error('error: yus-jsonapi.getResourceIdentifierObject requires a model.');
  if (!model.constructor) {
    var msg = 'error: yus-jsonapi.getResourceIdentifierObject requires a model to have constructor.';
    throw new Error(msg);
  }
  if (!model.constructor.type) {
    var msg = 'error: yus-jsonapi.getResourceIdentifierObject requires a model to have type.';
    throw new Error(msg);
  }
  if (!model.attributes) {
    var msg = 'error: yus-jsonapi.getResourceIdentifierObject requires a model to have attributes.';
    throw new Error(msg);
  }
  if (!model.attributes[model.idAttribute]) {
    var msg = 'error: yus-jsonapi.getResourceIdentifierObject requires a model to have id.';
    throw new Error(msg);
  }
  return {
    type: model.constructor.type,
    id: model.attributes[model.idAttribute].toString()
  }
};

// http://jsonapi.org/format/#document-resource-object-relationships
function getRelationshipLinksObject(model, relationshipName) {
  let relationshipLinksObject = {};
  let links = getResourceLinksObject(model);
  relationshipLinksObject.self = links.self + `/relationships/${relationshipName}`;
  relationshipLinksObject.related = links.self + `/${relationshipName}`;

  return relationshipLinksObject;
}

// Replace placeholders with corresponding parentIds
function substituteParentIds(path, parentIds) {
  _.forEach(parentIds, (id, type) => {
    path = path.replace(`:${type}_id`, id);
  });

  return path;
}

// http://jsonapi.org/format/#document-resource-objects
function getResourceLinksObject(model) {
  let links = {};


  // get parent ids
  let parentIds = {};
  let currentModel = model;
  while (currentModel.parentModel) {
    parentIds[currentModel.parentModel.constructor.type] = currentModel.parentModel.id;
    currentModel = currentModel.parentModel;
  }
  _.each(model.reqParams, (id, type_id) => {
    parentIds[type_id.replace('_id', '')] = id;
  });

  if (!model.constructor.collectionPath) {
    var msg = "ERROR: No collectionPath";
    console.log('msg:', msg, 'model:', model);
    throw new Error(msg);
  }

  let collectionPath = substituteParentIds(model.constructor.collectionPath, parentIds);
  links.collection = `${getBaseUrl()}/${collectionPath}`;
  links.self = `${getBaseUrl()}/${collectionPath}/${model.attributes[model.idAttribute]}`;

  // Special resource links function e.g. asset links
  if (!_.isEmpty(model.links)) {
    _.map(model.links, function(getLinkFunction, linkName) {
      links[linkName] = getLinkFunction(model);
    });
  }

  return links;
}

function getAttributesObject(model) {
  return _.omit(model.toJSON({ shallow: true }), model.idAttribute);
}

function relationshipIsToOne(relationship) {
  var one2one = ['belongsTo', 'hasOne', 'morphOne'];
  var isOne2one = _.indexOf(one2one, relationship.relatedData.type) > -1;
  return isOne2one;
}

function getRelationshipsObject(model) {
  let relationshipsObject = {};

  _.forEach(model.relationships, (relationship, relationshipName) => {

    relationshipsObject[relationshipName] = {};
    relationshipsObject[relationshipName].links = getRelationshipLinksObject(model, relationshipName);

    // all relationships are defined in model.relationships (these are just the functions like return this.hasMany, etc.)
    // included relationships are defined in model.relations (these are collections, like result sets)
    if (model.relations[relationshipName]) {
      if (relationshipIsToOne(model.relations[relationshipName])) {
        relationshipsObject[relationshipName].data = getResourceIdentifierObject(model.relations[relationshipName]);
      } else {
        relationshipsObject[relationshipName].data = getResourceIdentifierObjects(model.relations[relationshipName].models);
      }
    }
  });
  return relationshipsObject;
};


function gatherIncludesForEach(models, includes, parentModel, hashOfIncludes = {}) {
  _.forEach(models, function(model) {
    includes = gatherIncludes(model, includes, parentModel, hashOfIncludes);
  });

  return includes;
}

function gatherIncludes(model, includes, parentModel, hashOfIncludes = {}) {
  // Set the parent model for creating links later
  if (parentModel) {
    model.parentModel = parentModel;
  }

  // Get the resource object
  let resourceObject = getResourceObject(model);

  if (!hashOfIncludes[`${resourceObject.type}${resourceObject.id}`]) {
    includes.push(resourceObject);
    _.forEach(model.relations, function(relationship, relationshipName) {
      if (relationshipIsToOne(relationship)) {
        gatherIncludes(relationship, includes, model, hashOfIncludes);
      } else {
        gatherIncludesForEach(relationship.models, includes, model, hashOfIncludes);
      }
    });
    hashOfIncludes[`${resourceObject.type}${resourceObject.id}`] = true;
  }

  return includes;
}

function omitPrimaryFromIncludes(primaryData, includes) {
  let newIncludes = [];

  if (!_.isArray(primaryData)) {
    _.remove(includes, includedResource => {
      return includedResource.type === primaryData.type && includedResource.id === primaryData.id;
    });
    newIncludes = includes;
  } else {
    let primaryDataResourceType = primaryData[0].type;
    newIncludes = _.filter(includes, includedResource => includedResource.type !== primaryDataResourceType);
  }

  return newIncludes;
}











/**
 * JSONAPIify a bookshelf model.
 * Requires:
 *
 * - Model.type (Defined on each model)
 * - res.data (Bookshelf model)
 *
 * @param {[[Type]]} req  [[Description]]
 * @param {[[Type]]} res  [[Description]]
 * @param {[[Type]]} next [[Description]]
 */
function toJSONAPI(bookshelfModel, req, config = null) {
  API_ADDRESS = req.apiAddress; //Set global constant to be used by getBaseUrl

  // putting ?raw=true will cause the response w/o any toJSONAPIifying happening
  if (req.query.raw) {
    return bookshelfModel.toJSON();
  }

  // Top-level document
  // http://jsonapi.org/format/#document-top-level
  let topLevelDocument = {};
  let primaryData = null;
  let includes = [];

  // Primary Data object
  /*
   * http://jsonapi.org/format/#document-top-level
   * Primary data MUST be either:
   *
   * - a single resource object, a single resource identifier object, or null, for requests that target single resources
   * - an array of resource objects, an array of resource identifier objects, or an empty array ([]), for requests that target resource collections
   */
  if (bookshelfModel) {
    // There's multiple resources in the result listing. eg /channels, /channels/4/schedules, /channels/4/relationships/schedules
    if (bookshelfModel.models) {
      _.forEach(bookshelfModel.models, model => {
        model.reqParams = req.params;
      });

      if (isRelationshipsEndpoint(req)) {
        primaryData = getResourceIdentifierObjects(bookshelfModel.models);
      } else {
        primaryData = getResourceObjects(bookshelfModel.models);
        if (req.query.include) {
          includes = gatherIncludesForEach(bookshelfModel.models, includes);
        }
      }

      // there's only one resource in the result listing. eg /channels/4, /locations/5/timezone, channels/4?include=schedules
    } else {
      if (req.method === 'POST' && _.isArray(bookshelfModel)) { // may return an array 'cause it was POSTed to a collection endpoint, even though only one resource was added
        bookshelfModel = _.first(bookshelfModel);
      }

      bookshelfModel.reqParams = req.params;

      if (isRelationshipsEndpoint(req)) {
        primaryData = getResourceIdentifierObject(bookshelfModel);
      } else {
        bookshelfModel.path = req.path;
        primaryData = getResourceObject(bookshelfModel);
        if (req.query.include) {
          includes = gatherIncludes(bookshelfModel, includes);
        }
      }
    }

    if (req.query.include && !_.isEmpty(includes)) {
      includes = omitPrimaryFromIncludes(primaryData, includes);
    }
  }

  topLevelDocument.data = primaryData;

  if (req.query.includeId) {
    let toRemoveFromIncluded = req.query.includeId.split(',');
    let includedRelationships = (req.query.include) ? req.query.include.split(',') : null;
    toRemoveFromIncluded = _.pullAll(toRemoveFromIncluded, includedRelationships);

    if (config.relationshipNameTypes) {
      toRemoveFromIncluded = _.map(toRemoveFromIncluded, relationshipName => {
        return (config.relationshipNameTypes[relationshipName]) ? config.relationshipNameTypes[relationshipName] : relationshipName;
      });
    }

    includes = _.remove(includes, (includeObject) => !toRemoveFromIncluded.includes(includeObject.type));
  }

  if (req.query.include) {
    topLevelDocument.included = includes;
  }

  topLevelDocument.links = { self: getBaseUrl() + req.url };

  validateJsonApi(topLevelDocument, req.method, false);
  return topLevelDocument;
}












function toJSON(req, res, next) {
  validateJsonApi(req.body, req.method, true);
  //TODO: Batch requests. For now assume single data sets for post and patch.
  if (['POST', 'PATCH', 'PUT'].includes(req.method) && !_.isEmpty(req.body.data)) {
    req.data = req.body.data; //TODO: relationships handling. At the moment, this does the trick for top level resources.
  }

  if (req.data) {
    req.data = req.data.attributes;
  }

  return next();
}










function toJsonApiError({ id, links, status, code, title, detail, errorSource, meta, stack }) {
  return {
    errors: [{
      id,
      links: {
        about: links
      },
      status,
      code,
      title,
      detail,
      source: {
        pointer: errorSource
      },
      meta
    }]
  };
}






function validateJsonApi(body = {}, method = null, request = true) {
  // console.log("SKIPPING VALIDATION");
  return true;

  const methodsToParse = ['GET', 'POST', 'PUT', 'PATCH', 'DELETE']
  const validEmptyMethods = ['GET', 'DELETE']; // Can have no body
  const createMethods = ['POST', 'PUT']; // Only methods that send a full resource
  const updateMethods = ['PATCH'] // Only methods that sends a partial object

  if (_.isEmpty(body) && _.includes(validEmptyMethods, method)) return true;
  if (_.includes(methodsToParse, method) || method === null) {
    try { // validator throws error to indicate invalid jsonapi.
      if (request) {
        if (_.includes(createMethods, method)) {
          createValidator.validate(body);
        } else if (_.includes(updateMethods, method)) {
          updateValidator.validate(body);
        } else {
          regularValidator.validate(body);
        }
      } else {
        // responses use general schema
        regularValidator.validate(body);
        // valid JSON API.
      }
    } catch (e) {
      // invalid JSON API.
      let msg = {
        msg: 'error: body is not valid json',
        details: '',
        body: body,
        method: method,
        errorObject: e
      };
      /* errors is an array of objects containing details on each issue within the json
       *   It will compound issues however, eg a missing ID might lead to it thinking a relationship was trying to be nulled/set as well.
       *   Extract what is needed for the response.
       */
      e.errors.forEach(function(error) {
          let err = error;
          _.unset(err, 'data')
            // console.log(err)
          if (error.keyword == 'required') {
            msg.details += error.message + "\n";
          }
        })
        // so we can see the full error obj returned.
        // This data will need to be extracted and put into the SB2 error format
        // console.log(util.inspect(msg, false, null))
      throw new Error(msg.msg + msg.details)
    }
  }
  return true;
}


module.exports = { toJSON, toJSONAPI, toJsonApiError };
